#TDD (Desenvolvimento guiado por teste)

class AvengerHeadQuarter
  attr_accessor :list

  def initialize
    self.list = []
  end

  def put(avanger)
    self.list.push(avanger)
  end
end

describe AvengerHeadQuarter do

  it 'deve adicionar um vingador' do
    hq = AvengerHeadQuarter.new
    hq.put('Spiderman')
    expect(hq.list). to eql ['Spiderman']
  end

  it 'Deve adicionar uma lista de vingadores' do
    hq = AvengerHeadQuarter.new

    hq.put('Thor')
    hq.put('Hulk')
    hq.put('Spiderman')

    #expect(hq.list).to include 'Thor'

    res = hq.list.size > 0 #conta se os itens na lista e verifica se é maior que 0
    expect(hq.list.size).to be > 0 #confirma se existe itens na lista
    expect(res).to be true # espera que o resultado de res seja verdadeiro
  end

  it 'Thor deve ser o primeiro da lista' do
    hq = AvengerHeadQuarter.new

    hq.put('Thor')
    hq.put('Hulk')
    hq.put('Spiderman')

    expect(hq.list).to start_with('Thor')
  end

  it 'Iroman deve ser o último da lista' do
    hq = AvengerHeadQuarter.new

    hq.put('Thor')
    hq.put('Hulk')
    hq.put('Spiderman')
    hq.put('Iroman')

    expect(hq.list).to end_with('Iroman')
  end

  it 'Deve conter o sobrenome' do
    avenger = 'Peter Parker'

    expect(avenger).to match(/Parker/) # contem o nome Parker na string avenger?
    expect(avenger).not_to match(/Papito/) # contem o nome Papito na string avenger?
  end

end