require_relative '../../app/bank'


describe ContaPoupanca do

  describe 'Saque' do
    context 'quando o valor é positivo' do
      before(:all) do
        @cp = ContaPoupanca.new(1000.00)
        @cp.saca(200.00)
      end
      it 'então atualiza saldo' do
        expect(@cp.saldo).to eql 798.00
      end
    end

    context 'quando saldo é zero' do
      before(:all) do
        @cp = ContaPoupanca.new(0.00)
        @cp.saca(100.00)
      end
      it 'então exibo mensagem' do
        expect(@cp.mensagem).to eql 'Saldo insuficiente para saque :('
      end
      it 'então meu saldo continua sem alteração' do
        expect(@cp.saldo).to eql 0.00
      end
    end

    context 'quando saldo é insuficiente' do
      before(:all) do
        @cp = ContaPoupanca.new(500.00)
        @cp.saca(501.00)
      end
      it 'então exibo mensagem' do
        expect(@cp.mensagem).to eql 'Saldo insuficiente para saque :('
      end
      it 'então meu saldo continua sem alteração' do
        expect(@cp.saldo).to eql 500.00
      end
    end

    context 'quando existe um limite por saque' do
      before(:all) do
        @cp = ContaPoupanca.new(1000.00)
        @cp.saca(501.00)
      end
      it 'então exibo mensagem' do
        expect(@cp.mensagem).to eql 'Limite máximo por saque é de R$ 500'
      end
      it 'então meu saldo continua sem alteração' do
        expect(@cp.saldo).to eql 1000.00
      end
    end
  end
end