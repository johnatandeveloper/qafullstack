class Conta
  attr_accessor :saldo, :nome

  def initialize(nome) #inicializando a conta com saldo 0
    self.saldo = 0.0
    self.nome = nome
  end

  def deposita(valor)
    # "self" como se fosse o "this." do "C#"
    self.saldo += valor
    puts "Depositando a quantia de #{valor} reais na conta de #{self.nome}." #forma mais elegante de converter (.to_s)
  end
end

#quando crio a class, o metodo initialize já é iniciado.
c = Conta.new('Papito')

puts "#{c.nome}, seu saldo é de: #{c.saldo}"
c.deposita(100.00)
puts "#{c.nome}, seu saldo é de: #{c.saldo}"
c.deposita(10)
puts "#{c.nome}, seu saldo é de: #{c.saldo}"