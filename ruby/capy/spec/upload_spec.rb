

describe 'upload de arquivo', :upload do
  before(:each) do
    visit 'https://training-wheels-protocol.herokuapp.com/upload'
    @arquivo = Dir.pwd + '/spec/fixtures/arquivo.txt'#retorna o caminho onde eu tenho o diretorio de execução do meu projeto
    @imagem = Dir.pwd + '/spec/fixtures/groot.jpg'
  end

  it 'upload com arquivo de texto' do
    attach_file('file-upload', @arquivo)
    click_button 'Upload'
    @arquivo = find('#uploaded-file')
    expect(@arquivo.text).to eql 'arquivo.txt'
  end

  it 'upload com arquivo jpg' do
    attach_file('file-upload', @imagem)
    click_button 'Upload'
    img = find('#new-image')
    expect(img[:src]).to include '/uploads/groot.jpg' # confirma a informação no SRC da imagem (ULR)
  end

  after(:each) do
    sleep 3
  end
end