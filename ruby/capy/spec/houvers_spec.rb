

describe 'Hovers', :hovers do

  before(:each) do
    visit 'https://training-wheels-protocol.herokuapp.com/hovers'
  end

  it 'quando passo o mouse sobre o blade' do
    cardBlade = find('img[alt=Blade]')
    cardBlade.hover

    expect(page).to have_content 'Nome: Blade'
  end

  it 'quando passo o mouse sobre o pantera negra' do
    cardPantera = find('img[alt="Pantera Negra"]')
    #atributo com caracter especial ou espaço, precisa estar dentro de aspas
    cardPantera.hover

    expect(page).to have_content 'Nome: Pantera Negra'
  end

  it 'quando passo o mouse sobre o homem aranha' do
    cardHomemAranha = find('img[alt="Homem Aranha"]')
    #atributo com caracter especial ou espaço, precisa estar dentro de aspas
    cardHomemAranha.hover

    expect(page).to have_content 'Nome: Homem Aranha'
  end

end