# -- Tipos Ruby --
# O tipo de uma variável é definido no momento em que a mesma recebe o valor
# ou seja, tipagem dinâmica
# EX:

produto = 'Macbook Air'
preco = 5999.99
quantidade = 10
disponivel = true

# Para saber o tipo da variável, basta executar o comando abaixo:

puts produto.class
puts preco.class
puts quantidade.class
puts disponivel.class

# Para imprimir os valores, basta realizar o comando abaixo:
#
puts produto
puts preco
puts quantidade
puts disponivel
