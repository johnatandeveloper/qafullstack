
# -- Diga Ola --
def diga_ola(nome)
  puts 'Olá, ' + nome
end
puts ''
diga_ola('Johnatan!')


# -- Retorna Nome --
puts ''
def retorna_nome
    'Fernando'
end

resultado = retorna_nome
puts resultado
puts ''


# -- Soma --
def soma(v1, v2)
  total = v1 + v2
  total
end

res = soma(10, 25)
puts res
puts ''
