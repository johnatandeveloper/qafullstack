# Objeto que temos uma coleção de dados, onde temos chaves e valor.
#
carro = Hash[nome: 'Civic', marca: 'Honda', cor: 'Vermelho']
carro[:modelo] = 'SI' # Adicionando dado ao hash
puts carro
puts carro[:nome] # imprimindo só o valor desejado
