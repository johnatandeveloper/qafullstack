# -- Repetindo por Nx --
# |i| i.to_s * numera a quantidade de vezes que o loop está sendo repetido
#
puts ''
puts '----------'
puts '.times'
puts '----------'

5.times do |i|
  puts 'Repetindo a mensagem' + i.to_s + 'vez(es).'
end
puts ''
puts '----------'
puts 'while'
puts '----------'

init = 0
while init <= 10 do
  puts 'Lopping infinito' + init.to_s + 'vez(es).'
  init += 1 #incremento para evitar o loop infinito, somando 1 a cada sequencia, até chegar a 10.
end
puts ''
puts '----------'
puts 'for'
puts '----------'

for item in (0...10)
  puts 'Repetindo a mensagem' + item.to_s + 'vez(es).'
end
puts ''
puts '----------'
puts 'array'
puts '----------'

# -- Arrays --
#
vingadores = ['Iroman', 'Hulk', 'Spiderman']
vingadores.each do |v|
  puts v
  # 'V' é a variavel que recebe as informações do array 'vingadores'. O processo percorre até não ter mais dados
end