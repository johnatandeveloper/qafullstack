numero1 = 0
numero2 = 0
total = 0

# -- Calculadora --
#
# gets.chomp pega a informação digitada no terminal
# .to_i converte a string em inteiro
#

puts 'Informe o número 1:'
numero1 = gets.chomp.to_i

puts 'Informe o número 2:'
numero2 = gets.chomp.to_i

total = numero1 + numero2
total = numero1 - numero2
total = numero1 * numero2
total = numero1 / numero2
puts ''
puts total
puts ''

# -- Operadores de Comparação --
#
v1 = 10
v2 = 11

puts v1 > v2 #maior
puts v1 < v2 #menor
puts v1 >= v2 #maiorOuIgual
puts v1 <= v2 #menorOuIgual
puts v1 != v2 #diferente
puts v1 == v2 #igual
puts v1.eql?(v2) #igual





